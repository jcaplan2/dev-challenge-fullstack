package com.rli.cars.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.rli.cars.controller.CarResource;

@Component
@ApplicationPath("/app")
public class JerseyConfiguration extends ResourceConfig {
	 public JerseyConfiguration() {
		  register(CarResource.class);
		  register(MultiPartFeature.class);
	 }
}
