package com.rli.cars.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.rli.cars.exception.ResourceNotFoundException;
import com.rli.cars.model.Car;
import com.rli.cars.repository.CarRepository;

@Component
@Path("/cars")
public class CarResource {
	@Autowired
	private CarRepository carRepository;

	@GET
	@Produces("application/json")
	public List<Car> getAllCars() {
		List<Car> result = new ArrayList<Car>();
		carRepository.findAll().forEach(result::add);
		return result;
	}

	@GET
	@Produces("application/json")
	@Path("{id}")
	public ResponseEntity<Car> getCarById(@PathParam(value = "id") Long id) throws ResourceNotFoundException {
		return ResponseEntity.ok().body(
				carRepository.findById(id).orElseThrow(
						() -> new ResourceNotFoundException("Car not found: " + id)));
	}
}