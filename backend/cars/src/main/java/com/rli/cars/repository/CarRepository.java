package com.rli.cars.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.rli.cars.model.Car;

public interface CarRepository extends CrudRepository<Car, Long> {
	List<Car> findByMake(String make);
	
	Car findById(long id);
}