INSERT INTO car (make, model, year, weight, acceleration, image) VALUES
	('Tesla', 'Model 3', 2018, 4250, 3.1, null),
	('Tesla', 'Model X', 2016, 5390, 2.6, null),
	('Tesla', 'Model S', 2021, 4766, 1.9, null),
	('Tesla', 'Model Y', 2020, 4555, 3.5, null),
	('Porsche', 'Taycan', 2022, 4568, 3.0, null),
	('Ford', 'Mustang Mach E', 2022, 4394, 3.5, null),
	('Volkswagen', 'ID.4', 2021, 4665, 5.1, null),
	('Hyundai', 'Ioniq 5', 2022, 4432, 5.5, null),
	('Kia', 'Soul EV', 2020, 4555, 5.5, null),
	('Rivian', 'R1T', 2022, 7148, 3.2, null),
	('Nissan', 'Leaf', 2020, 4232, 6.0, null),
	('Lucid', 'Air', 2022, 5050, 2.5, null),
	('Audi', 'e-tron', 2020, 5721, 5.5, null),
	('Polestar', '2', 2022, 4680, 4.0, null),
	('Jaguar', 'I-Pace', 2018, 5886, 4.5, null),
	('Chevrolet', 'Bolt', 2017, 3563, 6.5, null);
	
