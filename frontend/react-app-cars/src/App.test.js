import { render, screen } from '@testing-library/react';
import CarApp from './components/CarApp';

test('renders learn react link', () => {
  render(<CarApp />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
