import React, { useEffect, useState } from "react";
import { Button, Card, Col, Container, Navbar, Row } from "react-bootstrap";
import { getCars } from "../services/CarService";
import "./CarApp.css";
import placeHolderImage from '../images/placeholder.jpg';

const CarApp = () => {
  const [cars, setCars] = useState([]);
  const [height, setHeight] = useState(500);

  useEffect(() => {
    loadCars();
    
    window.addEventListener('resize', resizeHeight);
    resizeHeight();
  }, []);

  const loadCars = () => {
    getCars()
      .then(res => {
        setCars(res.data);
      })
      .catch(err => console.log(err));
  }

  const resizeHeight = () => {
    setHeight(window.innerHeight - 56);
  }

  const renderCars = () => {
    let carGrid = [];
    let carRow = [];
    cars.forEach((car) => {
      carRow.push(car);
      if (carRow.length > 2) {
        carGrid.push([...carRow]);
        carRow = [];
      }
    });
    if (carRow.length > 0) carGrid.push(carRow);

    return carGrid.map((carRow, rowIndex) => 
      <Row key={rowIndex}>
        {carRow.map(car => 
          <Col key={car.id} xs={4}>
            <Card>
              <Card.Img variant="top" src={car.image ? "data:image/png;base64," + car.image : placeHolderImage} />
              <Card.Body>
                <Card.Title>{car.year} {car.make} - {car.model}</Card.Title>
                <Card.Text>
                  <Row>
                    <Col>
                      <Button>Edit</Button>
                    </Col>
                    <Col>
                      <Button>Image</Button>
                    </Col>
                    <Col>
                      <Button>Delete</Button>
                    </Col>
                  </Row>
                  <div>Weight: {car.weight} lbs</div>
                  <div>0-60: {car.acceleration} seconds</div>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        )}
      </Row>
    );
  }

  return (
    <div id="carapp">
      <Container className="main-container">
        <Navbar bg="light" expand="lg">
          <Container>
            <Navbar.Brand href="#home">Cars App</Navbar.Brand>
            <div className="navbar-actions">
              <Button>Add</Button>
            </div>
          </Container>
        </Navbar>
        <div className="car-list-container" style={{ height: height }}>
          {renderCars()}
        </div>
      </Container>
    </div>
  );
}

export default CarApp;