import React, { useState } from "react";
import { Button, Col, Form, Modal, Row } from "react-bootstrap";
import { updateCarImage } from "../services/CarService";

const ImageUploadModal = (props) => {
  const isOpen = props.isOpen;
  const handleClose = props.handleClose;
  const carId = props.car ? props.car.id : undefined;

  const [imageFile, setImageFile] = useState(undefined);

  const handleUploadImageFile = () => {
    updateCarImage(carId, imageFile)
      .then(res => {
        console.log("success");
        handleClose();
      }).catch(err => {
        console.log("error");
      });
  }

  return (
    <Modal show={isOpen} onHide={handleClose} className="car-modal">
      <Modal.Header closeButton>
        <Modal.Title>Upload Car Image</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col xs={3}>
            <label>Image</label>
          </Col>
          <Col xs={9}>
            <Form.Control type="file" onChange={(event) => setImageFile(event.target.files[0])} />
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick={handleUploadImageFile}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default ImageUploadModal;