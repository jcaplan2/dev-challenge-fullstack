Backend: Spring Boot w/ JPA, Hibernate, Jersey, and an H2 DB
Frontend: ReactJs w/ Axios, React-Bootstrap, Awesome Font
Environment Requirements: Java, NodeJs


Introduction: The application is a simple car database web application.  Cars are stored in the database with the following structure:
{
  id: auto-generated,
  make: string
  model: string,
  year: int,
  weight: int,
  acceleration: float,
  image: byte[] (nullable)
}


Getting Started:
1. Starting the backend:
  - Go to backend/cars
  - run mvnw.cmd spring-boot:run (windows) or ./mvnw spring-boot:run (linux) to start the web application
2. Starting the frontend:
  - Go to frontend/react-app-cars
  - Run npm install to install the required libs
  - Run npm start to start the ReactJs application


Exercises:

1. Add functionality to add, modify, and delete entries from the database to both the REST controller and the UI.

  Currently, the controller and UI only contain functionality for listing cars in the database.  Add the ability add a new car, modify an existing one, and delete an existing one to both the
controller and the UI.  For adding and modifying, the user should be able to specify the following fields: make, model, year, weight, and acceleration.  Do not worry about the image field.

2. Add searching and sorting functionality to both the REST controller and the UI.

  Add the ability to search across at least the 3 main fields (make, model, and year) for the cars as well as the ability to sort by any field in both ascending or descending order.  Add the 
corresponding UI elements (e.g. a search bar and sort parameter dropdown) and verify that they function correctly.  The sort parameter and order should be applied to the filtered search results as well.

Bonus: Add the ability to upload an image to the REST controller and the UI.

  Add the ability to upload an image file to be stored in the selected car's "image" field as a byte array.  Add the necessary UI elements and verify that it works.


Note: Feel free to use any resources (IDE, web browser, etc.) or any additional libraries to complete the exercises.
